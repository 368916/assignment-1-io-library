%define	SYS_READ	0
%define	SYS_WRITE	1
%define	SYS_EXIT	60
%define	STDIN		0
%define	STDOUT		1

section .text
; Принимает код возврата и завершает текущий процесс
exit:
	; arguments
	; rdi -- exit code
	; variables
	; rdi -- exit code
	mov rax, SYS_EXIT
	; mov rdi, rdi
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	; arguments
	; rdi -- string to find length of
	; variables
	; rdi -- pointer to next position
	mov	rax, rdi
.loop:
	cmp	byte [rdi], 0
	je	.end		; if 0 found
	inc	rdi
	jmp	.loop		; else
.end:
	sub	rdi, rax	;
	mov	rax, rdi	; rax -- string length
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	; arguments
	; rdi -- string to print
	; variables
	; rdi -- string to print
	push	rdi
	call	string_length
	pop	rdi
	mov	rdx, rax
	mov	rax, SYS_WRITE
	mov	rsi, rdi
	mov	rdi, STDOUT
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	; arguments
	; rdi -- char to print
	; variables
	; rdi -- char to print
	sub	rsp, 16
	mov	rax, rdi	;
	mov	[rsp], al	; saving char on stack

	mov	rax, SYS_WRITE	;
	mov	rsi, rsp	;
	mov	rdi, STDOUT	;
	mov	rdx, 1		;
	syscall			; printing

	add	rsp, 16
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	; arguments
	; -
	; variables
	; -
	mov	rdi, 0xA
	jmp	print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	; arguments
	; rdi -- number
	; variables
	; rax -- number
	; rsi -- base multiplyer
	; rcx -- pointer to next position
	; rdx -- remainder buffer
	sub	rsp, 64

	xor	rdx, rdx	; rdx = 0
	mov	rax, rdi	; rax -- number
	mov	rsi, 10		; rsi -- base multiplyer
	mov	rcx, rsp	;
	add	rcx, 63		; rcx -- pointer to next position
.divloop:
	div	rsi		; dividing to get remainder

	add	dl, '0'		; converting to character
	mov	[rcx], dl	; saving to the buffer
	xor	rdx, rdx	; emptying remainder

	dec	rcx		; moving to next position
	test	rax, rax
	jg	.divloop	; if number not ended

	inc	rcx		; reverting last 'dec'

	mov	rax, SYS_WRITE	;
	mov	rdi, STDOUT	;
	mov	rsi, rcx	; rsi -- pointer to start
	mov	rdx, rsp	;
	add	rdx, 64		;
	sub	rdx, rcx	; rdx -- number length
	syscall			; printing

	add	rsp, 64
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	; arguments
	; rdi -- number
	; variables
	; rdi -- number
	test	rdi, rdi
	jge	.print_uint	; if number >= 0

	neg	rdi		; number = -number
	push	rdi

	mov	rdi, '-'
	call	print_char	; printing sign character

	pop	rdi
.print_uint:
	jmp	print_uint	; printing unsigned integer

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	; arguments
	; rdi -- string1 pointer
	; rsi -- string2 pointer
	; variables
	; rdi -- string1 pointer
	; rsi -- string2 pointer
	; al -- buffer for string1's char
	; dl -- buffer for string2's char
	; rcx -- string length
	push	rdi
	push	rsi
	push	rsi
	call	string_length	; rax -- string1 length
	pop	rsi
	mov	rdx, rax	; rdx = rax

	mov	rdi, rsi
	call	string_length	; rax -- string2 length
	pop	rsi
	pop	rdi

	cmp	rax, rdx
	jne	.fail		; if lengths are different

	mov	rcx, rax	; rcx -- string length
	test	rcx, rcx
	je	.ok		; if lengths are 0

	xor	rax, rax	; rax = 0
	xor	rdx, rdx	; rdx = 0
.loop:
				; reading char from string1
	mov	al, [rdi + rcx - 1]
	 			; reading char from string2
	mov	dl, [rsi + rcx - 1]
	cmp	al, dl
	jne	.fail		; if chars are different

	dec	rcx		; moving to next position
	test	rcx, rcx
	jg	.loop		; if string not ended
				; else
.ok:
	mov	rax, 1		; rax = 1
	ret
.fail:
	xor	rax, rax	; rax = 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	; arguments
	; -
	; variables
	; rax -- char buffer
	sub	rsp, 16

	mov	rax, SYS_READ
	mov	rdi, STDIN
	mov	rsi, rsp
	mov	rdx, 1
	syscall			; reading character into [rsp]

	test	rax, rax
	je	.eof		; if EOF happend
	mov	al, [rsp]	; else, mov character into rax
.eof:
	add	rsp, 16
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	; arguments
	; rdi -- buffer pointer
	; rsi -- buffer size
	; variables
	; rdi -- buffer pointer
	; rsi -- position pointer
	; rcx -- buffer end pointer
	; rdx -- is word started
	mov	rcx, rdi	;
	add	rcx, rsi	; rcx = rdi + rsi
	dec	rcx		; rcx = rcx - 1 (for \0)
	mov	rsi, rdi	; rsi = rdi
	xor	rdx, rdx	; rdx = false
.loop:
	; reading character
	push	rdi
	push	rsi
	push	rcx
	push	rdx
	call	read_char
	pop	rdx
	pop	rcx
	pop	rsi
	pop	rdi
	; checking for EOF
	cmp	al, 0
	je	.ok		; if EOF
	; checking for whitespace
	cmp	al, 0x20
	je	.ws_yes		; if char == ' '
	cmp	al, 0x09
	je	.ws_yes		; if char == '\t'
	cmp	al, 0x0a
	je	.ws_yes		; if char == '\n'
	jmp	.ws_no		; else
.ws_yes:
	; if whitespace
	cmp	rdx, 0
	je	.loop		; if word not started
	jmp	.ok		; else
.ws_no:
	; if not whitespace
	mov	rdx, 1		; rdx = true
	mov	byte [rsi], al	; saving character
	inc	rsi		; setting next position
	cmp	rsi, rcx
	jl	.loop		; if buffer not ended
	jmp	.small_buf_err	; else
.ok:
	mov	byte [rsi], 0	; adding '\0' to the end
	mov	rax, rdi	; rax -- buffer start
	sub	rsi, rdi	; rsi -- word length
	mov	rdx, rsi	; rdx = rsi
	ret
.eof_err:
.small_buf_err:
	xor	rax, rax	; rax = 0
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	; arguments
	; rdi -- string pointer
	; variables
	; rdi -- string pointer
	; rsi -- position pointer
	; rax -- character buffer
	; rcx -- base multiplyer
	; rdx -- result
	mov	rsi, rdi	; rsi = rdi
	xor	rax, rax	; rax = 0
	xor	rdx, rdx	; rdx = 0
	mov	rcx, 10		; rcx = 10
.loop:
	; reading character
	mov	al, [rsi]
	; validating character
	cmp	al, '0'
	jl	.end		; if char < 0
	cmp	al, '9'
	jg	.end		; if char > 9
	; updating result
	cmp	al, 0
	push	rax
	je	.add_digit	; if number == 0
.multiply:
	push	rcx
	push	rdx
	mov	rax, rdx	; rax = rdx
	mul	rcx		; rax = rax * rcx
	pop	rdx
	pop	rcx
	mov	rdx, rax	; rdx = rax
	xor	rax, rax	; rax = 0
.add_digit:
	pop	rax
	sub	al, '0'		; converting code to digit
	add	rdx, rax	; result = result + digit
	; looping
	inc	rsi		; setting next position
	jmp	.loop		; going to loop's start
.end:
	mov	rax, rdx	; rax -- result
	sub	rsi, rdi	; rsi -- number length
	mov	rdx, rsi	; rdx = rsi
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	; arguments
	; rdi -- string pointer
	; variables
	; rdi -- string pointer
	; rax -- character buffer
	; rcx -- has sign flag
	xor rax, rax
	xor rcx, rcx
	; reading character
	mov	al, [rdi]
	; checking for sign
	cmp	al, '-'
	je	.signed		; if char == '-'
	jmp	.unsigned	; else
.signed:
	; if signed
	inc	rcx		; has flag = true
	inc	rdi		; rdi = rdi + 1
.unsigned:
	; if unsigned
	push	rcx
	call	parse_uint	; parsing unsigned integer
	pop	rcx
	; checking result
	cmp	rdx, 0
	je	.parse_err	; if error occured
	; applying sign
	cmp	rcx, 0
	je	.ok		; if sign flag == false
	neg	rax
	inc	rdx
.ok:
.parse_err:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	; arguments
	; rdi -- string pointer
	; rsi -- buffer pointer
	; rdx -- buffer length
	; variables
	; rdi -- string pointer
	; rsi -- buffer pointer
	; rdx -- string ending pointer
	; rax -- buffer
	push	rdi
	push	rsi
	push	rdx
	call	string_length
	pop	rdx
	pop	rsi
	pop	rdi		; finding string length
	inc	rax		; string length + 1 (\0)
	cmp	rdx, rax
	jl	.small_buf_err	; if len(buffer) < len(string)
	mov	rcx, rax	; rcx -- string length
	add	rcx, rdi	; rcx -- string ending pointer
.loop:
	; copying character
	mov	al, [rdi]
	mov	[rsi], al
	inc	rdi
	inc	rsi
	cmp	rdi, rcx
	jl	.loop		; if buffer not ended
	mov	rax, rcx	; rax = rcx
	sub	rcx, rsi	; rcx -- string length
	mov	rdx, rcx	; rdx -- string length
	jmp	.ok
.small_buf_err:
	xor	rax, rax	; rax = 0
.ok:
	ret
